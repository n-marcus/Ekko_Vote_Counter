void loadMovies() { 

  /// NOW LOOKING FOR MOVIES////
  int foundMovies = 0;
  for (int x = 0; x < battles; x ++ ) { 
    for (int y = 0; y < 2; y ++ ) {
      String file =str(x+1)+str(y+1)+".mov"; //file string
      println("looking for " + file);
      File f = new File(dataPath(file)); //to check if the file exists
      if (f.exists()) { //checking if such a file is there
        println("I think this file exists"); 
        movies[x][y] = new Movie(this, file); //loading the movie
      }
      if (movies[x][y] != null) { //print messages
        println("Found file : " + file); // movie is actually loaded and stuff
        foundMovies ++;
      } else if (movies[x][y] == null) {
        println("couldn't find movie " + file);
      }
    }
  }// end of movies

  if (foundMovies > 0) {
    showMessageDialog(null, "Sucessfully loaded " + foundMovies + " out of " + (battles * 2) + " movies!", "Found " + foundMovies + " movies!", INFORMATION_MESSAGE); //dispaying the found images
  } else if (foundMovies == 0) {
    showMessageDialog(null, "Couldn't find any of the movies I was looking for, check if they are located in a /data/ folder and end in .mov", "Didn't find any files", INFORMATION_MESSAGE);
  }
}


void askMaxVotes() {
  //create a popup asking for a number
  try { 
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
  } 
  catch (Exception e) { 
    println("canceled!");
    e.printStackTrace();
  } 

  String preset="50";
  String maxvotesString = JOptionPane.showInputDialog(frame, "Maximum number of expected votes?", preset);
  if (maxvotesString != null) maxvotes=Integer.parseInt(maxvotesString);
}


void askRounds() {
  //create a popup asking for a number
  try { 
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
  } 
  catch (Exception e) { 
    println("canceled!");
    e.printStackTrace();
  } 

  String preset="20";
  String numberOfBattlesString = JOptionPane.showInputDialog(frame, "How many rounds do you want?", preset);
  if (numberOfBattlesString != null) battles=Integer.parseInt(numberOfBattlesString);
}

void askInterval() {
  //create a popup asking for a number
  try { 
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
  } 
  catch (Exception e) { 
    println("canceled!");
    e.printStackTrace();
  } 

  String preset="1500";
  String intervalString = JOptionPane.showInputDialog(frame, "Timer duration in seconds? Default = 25 minutes", preset);
  if (intervalString != null) interval=(Integer.parseInt(intervalString) * 1000);
  println("User chose " + interval + " seconds");
}

void loadProfilePics() {
  int foundFiles = 0;
  //LOADING THE PROFILE PICS
  println("Now going to search for profile pics");
  for (int x = 0; x < battles; x ++ ) { //going to look for all files in a for loop
    for (int y = 0; y < 2; y ++ ) {
      String file ="kand"+(x+1)+(y+1)+".png";
      println("looking for " + file);
      pics[x][y] = loadImage(file); //loading the images in the array
      if (pics[x][y] == null) { //some debugging info
        println("Couldn't find file " + file);
      } else {
        println("Found a file named " + file);
        foundFiles ++;
        pics[x][y].resize(3 * (width / 10), int(height * 0.8));
      }
    }
  } //end of for loops

  if (foundFiles > 0) {
    showMessageDialog(null, "Sucessfully loaded " + foundFiles + " out of " + (battles * 2) + " profile pictures!", "Found " + foundFiles + " profile pictures!", INFORMATION_MESSAGE); //dispaying the found images
  } else if (foundFiles == 0) {
    showMessageDialog(null, "Couldn't find any of the profile pictures I was looking for, make sure they are located in the same directory as this executable", "Didn't find any profile pictures", INFORMATION_MESSAGE);
  }
}

void loadBackgroundMovie() {

  ///LOOKING FOR BACKGROUND MOV
  File backgroundMov = new File(dataPath("bg.mov")); //to check if the file exists
  println("Now looking for background file " + backgroundMov.getAbsolutePath());

  if (!backgroundMov.exists()) return;
  bg = new Movie(this, "bg.mov");
  println("Found and loaded bg.mov");
  if (bg != null) {
    showMessageDialog(null, "Sucessfully loaded the MOV background image", "Found MOV background", INFORMATION_MESSAGE); //dispaying the found images
    bg.loop();
  } else {
    println("couldnt find the background file" );
    showMessageDialog(null, "Didn't find any of the background images, make sure they are in the /data/ folder and have a .mov extension", "Didn't find background", INFORMATION_MESSAGE); //dispaying the found images
  }
}

void loadBackgroundGif() {
  //LOOKING FOR BACKGROUND GIF
  File backgroundGifFile = new File(dataPath("bg.gif")); //to check if the file exists
  println("Now looking for background file " + backgroundGifFile.getAbsolutePath());

  if (!backgroundGifFile.exists()) return;
  backgroundGif = new Gif(this, "bg.gif");
  println("loaded animated gif");
  backgroundGiffirstframe = Gif.getPImages(this, "bg.gif");
  println("Found and loaded bg.gif and first frame");
  if (backgroundGif != null) {
    showMessageDialog(null, "Sucessfully loaded the GIF background", "Found GIF background", INFORMATION_MESSAGE); //dispaying the found images
    backgroundGif.loop();
  } else {
    println("couldnt find the background file" );
    showMessageDialog(null, "Didn't find any of the background images, make sure they are in the /data/ folder and have a .gif extension", "Didn't find background", INFORMATION_MESSAGE); //dispaying the found images
  }
}

void askGifOrMov() {
  //asking if the user wants to use a gif or a mov background 
  int dialogResult = JOptionPane.showConfirmDialog(null, "Do you want to use a GIF background?", "GIF Background?", JOptionPane.YES_NO_OPTION);
  if (dialogResult == JOptionPane.YES_OPTION) {
    println("user chose gif ");
    bggif = true;
  } else {
    println("User wants mov");
    bggif = false;
  }
}

