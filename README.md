# Ekko_Vote_Counter
This is a program to count votes made in Processing
It includes a nice bar and droplet class which can be easily reused to create a vertical slider.
The droplet class can make a nice droplet when the startDrop() method of it is called.

You can adjust the amount of votes by pressing either "w" and "s" or "o" and "l".

You can customize the appearance by placing files with the right name and extension in the /data/ folder.
The "profile pictures" of each candidate in each round should be named "kandxy.png". Where x is the round and y is the player number. 
So a file named "kand32" will be displayed as the second candidate of the third round.

At the end of each round a movie is displayed depending on which candidate won the last round. The naming is as follows: "xy.mov". Where x is the round and y is the kandidate number. 
For example: a file named "42.mov" will be displayed when the second candidate of the fourth round has the most votes.

To customize the background you need a file named either "bg.mov" or "bg.gif". At the setup of the program you get the option to choose either the .gif or the .mov version of bg.

![alt tag](http://i.imgur.com/HPHPQuV.jpg)
