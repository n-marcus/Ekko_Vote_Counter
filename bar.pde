class Bar {
  int votes = 0; 
  int x, y;
  int ydistance, xdistance;
  int textoffset;
  int maxVotes = 10, votelength;
  PFont f2;
  boolean moving = false;
  Drop myDrop;
  float extraThickness;


  Bar(int x, int y, int xdistance, int maxVotes) {
    //constructor
    myDrop = new Drop(int(random(255)), 200, 100, width/2, height/2, 10); //making a droplet
    this.x = x;
    this.y = y;
    this.xdistance = xdistance;
    println("x, y" + x + ", " + y);
    this.maxVotes = maxVotes; 
    this.textoffset =  xdistance - 10;
    if (maxVotes != 0) {
      votelength = (height  - (height - y))/maxVotes;
    }
    println("height - y = " + (height - y));
    println("votelength = " + votelength);

    // Create the font
    //printArray(PFont.list());
    f2 = createFont("gunplay", 70);
  } 

  void draw() {
    myDrop.update(); // make sure the droplet gets drawn


    //DRAWING THE BAR//
    calcPositions();
    calculateColors(); //here we calculate a gradient from red to green depending on the amount of votes
    //stroke(0);
    //strokeWeight(0);
    rect(x, y, xdistance + extraThickness, ydistance, 10, 10, 10, 10); //drawing the actual bar
    stroke(0);
    strokeWeight(0);

    //DRAWING THE TEXT
    fill(255);
    int text_y = constrain(y + (ydistance /2), 10, height); //making sure the text stays inside the box. 
    textFont(f2, xdistance + (votes/4));
    textAlign(LEFT, CENTER);
    text(votes, x + ((votes+ 1)/10), text_y); //drawing the text displauing the number of votes
  }

  void votePlus() {
    votes ++;
    myDrop.startDrop(int(random(255)), 255, 255, x + (xdistance + extraThickness)/2, y + ydistance - 10, 20); 
    println("Plus one vote!, vote amout is now: " + votes);
  }

  void voteMin() {
    if (votes > 0) {
      votes --;
      println("Min one vote!, vote amout is now: " + votes);
    } else {
      println("You can't have negative votes, can you?");
    }
  }

  void calculateColors() {
    float process = float(votes) / float(maxVotes); //calculate how far we are in the bar
    process = process * 90;
    //wprintln(process);
    //fill(process, 255, 255);
    colorMode(RGB);
    fill(145, 49, 77);
  }

  void calcPositions() {
    extraThickness = votes /2; // calculating how thick the bar should be
    int goaldistance = -(votes * votelength); //calculating the distance from the bottom of the bar to the end
    //println("Barmoving = " + str(moving));
    //println("ydistance = " + ydistance + " goaldistance = " + goaldistance);
    if (ydistance == goaldistance && moving == true) { // if the current distance is not equal to the goal distance, adjust it untill it is equal. This gives problems when the speed is more than one as it might skip
      moving = false;
    } else if (ydistance < goaldistance) {
      moving = true;
      ydistance += 1;
      //println("moving bar up");
    } else if (ydistance > goaldistance) {
      moving = true;
      ydistance -= 1;
      //println("moving bar down");
    }
  }

  void reset() {
    votes = 0;
    ydistance = 0;
  }

  int getVotes() {
    println("Sending votes to master, votes = " + this.votes);
    return this.votes;
  }
}

