
//VARIABLES
int battles = 30;
int buttonsize = 20;
int maxvotes;
PFont f;
int currentRound = 0;
boolean flickering = false;
boolean bggif = true;
int winningPlayer = 0;
int lastRound = 0;
int interval;
int startTime;

//VIDEO
import processing.video.*;
Movie bg;
Movie [][] movies = new Movie[battles][2];
boolean moviePlaying = false;

//POPUP BUSINESS
import static javax.swing.JOptionPane.*;
import javax.swing.*; 


//COUNTDOWN BUSINES
import com.dhchoi.CountdownTimer;
import com.dhchoi.CountdownTimerService;
CountdownTimer timer;
CountdownTimer flickerTimer;
long timerCallbackInfo;

//GIF BUSINESS
import gifAnimation.*;
Gif backgroundGif;
PImage[] backgroundGiffirstframe;

//PICTURES
PImage[][] pics = new PImage[battles][2];


//TWO VOTING BARS
Bar bar1; 
Bar bar2;

void setup() {  

  size(displayWidth, displayHeight);
  //fullScreen(1); // use this for Processing 3
  //LOADING A FONT
  f = createFont("gunplay", 12);
  textFont(f);
  textAlign(LEFT);


  //INTEROGATION PROCESS
  askMaxVotes();
  //asking how many rounds there should be
  askRounds();
  askInterval();
  askGifOrMov();

  //FILE LOADER ///
  if (bggif) {
    loadBackgroundGif(); // loading mov or gif background
  } else {
    loadBackgroundMovie();
  }
  loadProfilePics();
  loadMovies();


  bar1 = new Bar((width/5) * 2, height - height/10, 75, maxvotes);
  bar2 = new Bar((width/5) * 3 - 20, height - height/10, 75, maxvotes); //constructing bars, see class for details on arguments
  
  //starting timer after files loaded preferably
  timer = CountdownTimerService.getNewCountdownTimer(this).configure(10, interval).start(); // lets go!
}


void draw() {
  frameRate(30);
  if (!moviePlaying) {


    colorMode(HSB); //pretty colors
    //background(0);


    //DRAWING THE BACKGROUND
    if (!bggif) { 
      if (bg != null) {
        image(bg, 0, 0);
      }
    } else if (backgroundGif != null) {
      //println("Displaying gif background");
      image(backgroundGiffirstframe[1],0,0,width,height);
      image(backgroundGif, 0, 0, width,height);
    }

    //DRAWING THE VOTE BARS
    bar1.draw();
    bar2.draw();

    //DISPLAYING REMAINING TIME
    colorMode(RGB);
    fill(255);
    textFont(f, 70);
    textAlign(CENTER);
    long currentTime = timerCallbackInfo / 1000;
    String seconds = nf(int(currentTime % 60), 2);
    String minutes = nf(int(currentTime / 60), 2);
    text( minutes + ":" + seconds, width/2, 100); 

    //text(timerCallbackInfo, width/2, height/2);
    //text(s, 10, 10, 220, 200); //instructions 

    //DRAWING THE ACTUAL PICTURES
    if (pics[currentRound][0] != null && pics[currentRound][1] != null && !flickering) { // only when there are actually pictures and when flickering is off 
      imageMode(CORNER);
      image(pics[currentRound][0], 100, (height - height/10)  - (pics[currentRound][0].height)); //drawing picture 1
      image(pics[currentRound][1], (width/3 * 2), (height - height/10) - pics[currentRound][1].height); //drawing picture 2

      // But what will you do when there are no pictures? Print it!
    } else if (pics[currentRound][0] == null && frameCount % 60 == 0 ) {
      println("couldn't find file pics[" +  currentRound + "][0]"); // picture 1 not found
    } 
    if  (pics[currentRound][0] == null  && frameCount % 60 == 0 ) {
      println("couldn't find file pics[" +  currentRound + "][1]"); //picture 2 not found
    }
  }


  if (moviePlaying && movies[lastRound][winningPlayer] != null ) { // playing movie and movie is not null
    println("Playing movie " + lastRound + " " + winningPlayer);
    if (movies[lastRound][winningPlayer].time() < movies[lastRound][winningPlayer].duration() - 2) { // if the movie has not yet ended 
      image(movies[lastRound][winningPlayer], 0, 0, width, height); //draw the movie
      println("should be displaying now: " + lastRound + " " + winningPlayer);
      println("Time = " + movies[lastRound][winningPlayer].time() + " of " + movies[lastRound][winningPlayer].duration());
    } else { // If the movie has ended
      moviePlaying = false; //set the boolean
      println("movie ended , moving along...");
      println("resetting timer to " + interval);

      //Reset the timer
      timer.reset(CountdownTimer.StopBehavior.STOP_AFTER_INTERVAL);
      timer.configure(10, interval).start();
    }
  } else {//end if movie playing and exists
    //println("movie doesn't exist, starting timer now");
    //timer.start();
  }  
  //text(frameRate, 10, 10); //testing the fps to make sure it keepps running smoothly



  if (frameCount % 30 == 0) {
    println("TIMER VALUE = " + timerCallbackInfo);
  }

  //makes the screen flicker at the right times


  flicker();
}



void keyPressed() {
  if (!moviePlaying) { 
    if (key == 'w') {
      bar1.votePlus();
    } else if (key == 's') {
      bar1.voteMin();
    } else if (key == 'o') {
      bar2.votePlus();
    } else if (key == 'l') {
      bar2.voteMin();
    } else if (key == ' ') {
      flickering();
    }
  }
  if (key == '.' && moviePlaying ) {
    movies[lastRound][winningPlayer].jump(movies[lastRound][winningPlayer].duration() - 2.);
  }
}

void movieEvent(Movie m) {
  m.read();
}

void nextRound() {
  println("BAR 1 HAS " + bar1.getVotes() + " VOTES");
  println("BAR 2 HAS " + bar2.getVotes() + " VOTES");
  if (bar1.votes > bar2.votes) {
    moviePlaying = true;
    winningPlayer = 0;
    println("PLAYER 1 WON");
    println("going to play movies[" + currentRound + "][0]");
    try {
      movies[currentRound][0].frameRate(30);
      movies[currentRound][0].play();
      println("playing the movie movies[" + currentRound + "][" + winningPlayer + "] worked ");
    }
    catch(NullPointerException n) {
      println("No movie loaded in movies[" + currentRound + "][" + winningPlayer + "]");
    }
  } else if (bar2.votes > bar1.votes) {
    moviePlaying = true;
    winningPlayer = 1;
    try {
      movies[currentRound][1].frameRate(30);
      movies[currentRound][1].play();
      println("playing the movie movies[" + currentRound + "][" + winningPlayer + "] worked ");
    }
    catch(NullPointerException n) {
      println("No movie loaded in movies[" + currentRound + "][" + winningPlayer + "]");
    }
    println("PLAYER 2 WON");
  }
  lastRound = currentRound;
  currentRound ++; 
  bar1.reset();
  bar2.reset();
}

void onTickEvent(CountdownTimer t, long timeLeftUntilFinish) {
  timerCallbackInfo = timeLeftUntilFinish;
}

void onFinishEvent(CountdownTimer t) {
  timerCallbackInfo = 0;
  println("Finished");
  flickering();
}

void flickering() { //starts the flickering 
  startTime = millis();
  flickering = true;
}

void flicker() { //draws the filickering
  if (flickering) {
    if (frameCount % 30 < 15) { // wait 15 frames and display white
      fill(255);
      rect(0, 0, width, height);
      fill(0);
      text("END OF ROUND " + (currentRound + 1), width/2, height/2);
      println("Flicker");
    } else {  //after 15 frames make everything inverted
      fill(0);
      rect(0, 0, width, height);
      fill(255);
      text("END OF ROUND " + (currentRound + 1), width/2, height/2);
    }
    if (millis() - startTime > 10000) { // when you're done flickering, move to the next round
      println("FLICKERING DONE");
      flickering = false;
      nextRound();
    }
  }
}

