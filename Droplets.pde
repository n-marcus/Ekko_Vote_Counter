class Drop {
  float size;
  int h, s, b, a = 255;
  float x, y;
  float stroke;
  boolean started = false;
  Drop(int h, int s, int b, float x, float y, float size) {
    //constructor
    this.h = h;
    this.s = s;
    this.b = b;
    this.x = x;
    this.y = y;
    this.size = size;
    this.a = 255;
    this.stroke = 20;
  }

  void update() {
    if (started) {
      strokeWeight(stroke);
      colorMode(HSB);
      noFill();
      stroke(h, s, b, a);
      ellipse(x, y, size, size);
      size += 4;
      s -= 2;
      b -= 2;
      a -= 4;
      if (stroke - 0.1 > 0 ) {
        stroke -= 0.1;
      }
      //    println(a);
    }
  }

  void startDrop(int h, int s, int b, float x, float y, float size) {
    started = true;
    this.h = h;
    this.s = s;
    this.b = b;
    this.x = x;
    this.y = y;
    this.size = size;
    this.a = 255;
    this.stroke = 10;
  }
}
